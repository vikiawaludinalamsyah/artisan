<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>
    <form action="/welcome" method="post">
        @csrf
    <label>First Name</label><br> <br>
    <input type="text" name="nama"><br><br>
    <label>Last Name</label><br> <br>
    <input type="text" name="nama"><br><br>
    <label>Gender : </label><br> <br>
    <input type="radio" name="Gender">Male <br>
    <input type="radio" name="Gender">Female <br>
    <input type="radio" name="Gender">Other <br><br>
    <label>Nationality</label><br><br>
    <select name="Nationality" id="">
        <option value="Indonesian">Indonesian</option>
        <option value="Arabian">Arabian</option>
        <option value="American">American</option>
        <option value="Inggris">Inggris</option>
        <option value="Russian">Russian</option>
        <option value="Brazil">Brazil</option>
    </select> <br><br>
    <label>Language Spoken :</label><br><br>
    <input type="Checkbox">Bahasa Indonesia <br>
    <input type="Checkbox">English <br>
    <input type="Checkbox">Other <br><br>
    <label> Bio : </label> <br><br>
    <textarea name="Bio" cols="30" rows="10"></textarea> <br><br>
    <button>Sign Up</button>
</form>
</body>
</html>